package kaltura.reactive.ws.conf;

import kaltura.reactive.ws.handler.ChatSocketHandler;
import kaltura.reactive.ws.handler.TopicSocketHandler;
import kaltura.reactive.ws.model.Event;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.TopicProcessor;
import reactor.core.publisher.UnicastProcessor;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class WSConf {

    @Bean
    public UnicastProcessor<Event> unicastEventPublisher(){
        return UnicastProcessor.create();
    }

    @Bean
    public TopicProcessor<Event> topicEventPublisher(){
        return TopicProcessor.create();
    }

    @Bean
    public Flux<Event> unicastEvents(UnicastProcessor<Event> unicastEventPublisher) {
        return unicastEventPublisher
                .replay(25)
                .autoConnect();
    }

    @Bean
    public Flux<Event> topicEvents(TopicProcessor<Event> topicEventPublisher) {
        return topicEventPublisher
                .replay(25)
                .autoConnect();
    }

//    @Bean
//    public RouterFunction<ServerResponse> routes(){
//        return RouterFunctions.route(
//                GET("/"),
//                request -> ServerResponse.ok().body(BodyInserters.fromResource(new ClassPathResource("public/index.html")))
//        );
//    }

    @Bean
    public HandlerMapping webSocketMapping(UnicastProcessor<Event> unicastEventPublisher, TopicProcessor<Event> topicEventPublisher, Flux<Event> unicastEvents, Flux<Event> topicEvents) {
        Map<String, Object> map = new HashMap<>();
        map.put("/websocket/chat", new ChatSocketHandler(unicastEventPublisher, unicastEvents));
        map.put("/websocket/topics", new TopicSocketHandler(topicEventPublisher, topicEvents));
        SimpleUrlHandlerMapping simpleUrlHandlerMapping = new SimpleUrlHandlerMapping();
        simpleUrlHandlerMapping.setUrlMap(map);
        simpleUrlHandlerMapping.setOrder(10);
        return simpleUrlHandlerMapping;
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }

//    @Bean
//    public UserStats userStats(Flux<Event> events, UnicastProcessor eventPublisher){
//        return new UserStats(events, eventPublisher);
//    }
}
