package kaltura.reactive.ws;

import kaltura.reactive.ws.model.Event;
import kaltura.reactive.ws.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Flux;

import java.net.URI;
import java.time.Duration;

public class Client {
    public static void main(String[] args) {
        connectAndRunMessage();
    }

    private static void connectAndRunMessage() {
        User[] users = new User[]{User.randomUser(), User.randomUser()};
        WebSocketClient client = new ReactorNettyWebSocketClient();
        client.execute(
                URI.create("ws://localhost:8080/websocket/topics"),
                session -> session.send(
                        Flux.range(0, 10)
                                .map(r -> session.textMessage(toJSON(
                                        createEvent(users[r%2])
                                )))
                                .delayElements(Duration.ofSeconds(1))

                )
                        .thenMany(session.receive()
                                .map(WebSocketMessage::getPayloadAsText)
                                .map(m -> " message: *****************************************" + m)
                                .log())
                        .then())
                .block(Duration.ofSeconds(100L));
    }

    private static Event createEvent(User user) {
        return Event.type(Event.Type.CHAT_MESSAGE)
                .withPayload().property("msg", "hello from user:" + user.getAlias()).property("roomId", "7")
                .withUser(user)
                .build();
    }

    static ObjectMapper mapper = new ObjectMapper();

    private static String toJSON(Event event) {
        try {
            return mapper.writeValueAsString(event);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
